﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour
{
    Scene scene;

    private float time;
    private int a = 0;
    private int b = 0;
    private int c = 0;
    private int d = 0;
    private int e = 0;
    private int f = 0;
    private int score;
    private int coutner = 1;
    private int coutner2 = 1;
    private bool is_Instructions;
    private bool gameWon;
    private string objectLimited;

    public GameObject ramp;
    public GameObject plane;
    public GameObject s_shape;
    public GameObject teleporter_in;
    public GameObject teleporter_out;
    public GameObject turbo;
    public GameObject jumper;
    public GameObject ramp2;
    public GameObject plane2;
    public GameObject s_shape2;
    public GameObject teleporter_in2;
    public GameObject teleporter_out2;
    public GameObject turbo2;
    public GameObject jumper2;
    public Text instructions;
    public Text levelDisplay;
    public Text warningText;
    public Font font;


    private GUIStyle style = new GUIStyle();
    private GUIStyle levelStyle = new GUIStyle();
    private GUIStyle instructionStyle = new GUIStyle();
    private GUIStyle unbeatenLevelStyle = new GUIStyle();

    private Rect optionRect = new Rect(20, 20, 260, 210);   //formatting for the displays
    private Rect instructionRect = new Rect(20, 240, 300, 70);
    private Rect levelRect = new Rect(Screen.width / 2, Screen.height / 4, 260, 100);
    private Rect chooseLevelRect = new Rect(20, 370, 350, 100);
    private Rect worldRect = new Rect(20, 500, 360, 150);
    private Rect timerRect = new Rect(Screen.width - 400, 20, 350, 100);

    private int maxObjects = 12;        //setting max number of objects in scene to 12
    private int usedObjects = 0;       
    private string buildTimer;
    private string playTimer;
    private bool allow = true;
    private bool haveShape2 = true;
    private bool haveShape3 = true;

    private void OnGUI()
    {

        style.fontSize = 25;            //more formatting for text that appears.
        style.font = font;
        style.normal.textColor = Color.white;
        style.alignment = TextAnchor.MiddleCenter;

        levelStyle.fontSize = 70;       //more formatting
        levelStyle.font = font;
        levelStyle.normal.textColor = Color.white;
        levelStyle.alignment = TextAnchor.MiddleCenter;

        instructionStyle.fontSize = 50;       //more formatting
        instructionStyle.font = font;
        instructionStyle.normal.textColor = Color.white;
        instructionStyle.alignment = TextAnchor.MiddleCenter;

        unbeatenLevelStyle.fontSize = 25;            //more formatting for text that appears.
        unbeatenLevelStyle.font = font;
        unbeatenLevelStyle.normal.textColor = Color.grey;
        unbeatenLevelStyle.alignment = TextAnchor.MiddleCenter;


        optionRect = GUILayout.Window(0, optionRect, DoMyWindow, "OBJECTS");

        instructionRect = GUILayout.Window(1, instructionRect, DoMyInstructionWindow, "INFO");

        chooseLevelRect = GUILayout.Window(3, chooseLevelRect, DoMyChooseLevelWindow, "STATS");

        worldRect = GUILayout.Window(4, worldRect, DoMyWorldWindow, "WORLDS");

        timerRect = GUILayout.Window(5, timerRect, DoMyTimerWindow, "TIMER");

        if (gameWon)            //after winning, this will display
        {
            levelRect = GUILayout.Window(2, levelRect, DoMyLevelWindow, " WOULD YOU LIKE TO CONTINUE?");
        }

        GUI.Box(new Rect(50, 50, Screen.width, Screen.height), levelDisplay.text, levelStyle); // Display level and number

        if (is_Instructions)
        {
            GUI.Box(new Rect(Screen.width / 3, Screen.height / 7, 900, 600), instructions.text, instructionStyle);
        }
    }

    private void Update()
    {
        levelDisplay = GameObject.Find("GameController").GetComponent<GameControllerScript>().level;        //checks game status

        gameWon = GameObject.Find("Player").GetComponent<PlayerScript>().winGame;

        score = GameObject.Find("GameController").GetComponent<GameControllerScript>().GetScore();

        if (GameObject.Find("GameController").GetComponent<GameControllerScript>().isBuildTimer())
        {
            buildTimer = GameObject.Find("GameController").GetComponent<GameControllerScript>().GetBuildTimer();
        }
        if (GameObject.Find("GameController").GetComponent<GameControllerScript>().isPlayTimer())
        {
            playTimer = GameObject.Find("GameController").GetComponent<GameControllerScript>().GetPlayTimer();
        }

        // Clear warning text after certain frames
        if (time < 1)
        {
            warningText.text = "";
        }
        time -= time * Time.deltaTime;
    }

    public void DoMyWindow(int windowID0)
    {
        scene = SceneManager.GetActiveScene();      //setting the maximum number of objects on each world.

        if (scene.name == "World 2")
        {
            maxObjects = 8;
        }
        if (scene.name == "World 3")
        {
            maxObjects = 6;
        }
     
        if (a + b + c + d + e + f >= maxObjects)        //no more objects if the combined total is greater than maxObjects
        {
            allow = false;
        }
        // Place Objects in screen depending on the button clicked
        if (GUILayout.Button("RAMP", style))        //spawning all of the different objects
        {
            if (allow)
            {
                if (a == 0)
                {
                    a = createObjects(ramp, a, true);
                }
                else if (a == 1 && scene.name != "World 3")
                {
                    a = createObjects(ramp2, a, true);
                }
            }
        }

        if (GUILayout.Button("PLANE", style))
        {
            if (allow && haveShape3)
            {
                if (b == 0)
                {
                    b = createObjects(plane, b, true);
                }
                else if (b == 1 && scene.name != "World 2")
                {
                    b = createObjects(plane2, b, true);
                }
            }
        }

        if (GUILayout.Button("S-SHAPE", style))
        {
            if (allow && haveShape2)
            {
                if (c == 0)
                {
                    c = createObjects(s_shape, c, true);
                }
                else if (c == 1)
                {
                    c = createObjects(s_shape2, c, true);
                }
            }

        }
        if (GUILayout.Button("JUMPER", style))
        {
            if (allow && haveShape3)
            {
                if (d == 0)
                {
                    d = createObjects(jumper, d, true);
                }
                else if (d == 1 && scene.name != "World 3")
                {
                    d = createObjects(jumper2, d, true);
                }
            }
        }
        if (GUILayout.Button("TURBO", style))
        {
            if (allow)
            {
                if (e == 0)
                {
                    e = createObjects(turbo, e, true);
                }
                else if (e == 1 && scene.name != "World 2")
                {
                    e = createObjects(turbo2, e, true);
                }
            }
        }
        if (GUILayout.Button("TELEPORTER", style))
        {
            if (allow && haveShape2)
            {
                if (f == 0)
                {
                    f = createObjects(teleporter_in, f, false);
                    f = createObjects(teleporter_out, f, true);
                    GameObject.Find("GameController").GetComponent<GameControllerScript>().penaltyTime();
                    // Display warning for selecting a teleport
                    warningText.text = "You have activated a teleport\n" +
                        "You have two minutes of play time\n" +
                        "to beat the level.\n" +
                        "You cannot move the teleporters more than 100 units\n" +
                        "or they will reset to their original position.\n" +
                        "Good luck!!";
                    time = 200;
                }
                else if (f == 1)
                {
                    f = createObjects(teleporter_in2, f, false);
                    f = createObjects(teleporter_out2, f, true);
                }
            }
        }
    }

    public int createObjects(GameObject name, int count, bool updateCount)
    {

        name.SetActive(true);       //setting the objects to active
        if (updateCount)
        {
            count++;
            usedObjects++;        // NEW LINE
        }
        return count;
    }

    public void DoMyInstructionWindow(int windowID1)        //instructions window.  not completed due in future iteration.
    {
        if (GUILayout.Button("OBJECT INSTRUCTIONS", style))
        {
            if (coutner % 2 != 0)
            {
                instructions.text =
                    "Click object then use Arrow Keys\n to move objects in the Xand Y direction.\n" +
                    "Click object then use 1 and 3\n to move objects along the Z-axis.\n" +
                    "Click object then \nuse 4, 5 and 6 to rotate objects.  " +
                    "Press P to play \nand R to restart.\n";
            }
            else
            {
                instructions.text = "";
            }
            coutner++;
            is_Instructions = true;

        }
        if (GUILayout.Button("CAMERA INSTRUCTIONS", style))
        {
            if (coutner % 2 != 0)
            {
                instructions.text = " Use W, A, S, D to move the camera.\n" +
                    "Press left click and hold to 'look around'\n" +
                    "Use the scroll wheel to zoom in and zoom out.\n";
            }
            else
            {
                instructions.text = "";
            }
            coutner++;
            is_Instructions = true;
        }

        if(scene.name == "World 1")
        {
            objectLimited = " Objects Limited: N/A";
        }
        else if (scene.name == "World 2")
        {
            objectLimited = " Objects Limited: 1 Plane & 1 Turbo";
        }
        else if (scene.name == "World 3")
        {
            objectLimited = " Objects Limited: 1 Ramp & 1 Jumper";
        }

        if (GUILayout.Button("WORLD DESCRIPTION", style))
        {
            if (coutner % 2 != 0)
            {
                instructions.text = " Amount of Objects Available: "+maxObjects+"\n" +
                                    "Spawns Per Object: 2\n" 
                                    + objectLimited;
            }
            else
            {
                instructions.text = "";
            }
            coutner++;
            is_Instructions = true;
        }
    }

    public void DoMyLevelWindow(int windowID2)      //display that shows up after beating a level
    {
        scene = SceneManager.GetActiveScene();

        if (GUILayout.Button("YES", style))
        {
            if (scene.name == "World 1")
            {
                SceneManager.LoadScene("World 2");
            }
            else if (scene.name == "World 2")
            {
                SceneManager.LoadScene("World 3");
            }
        }
        else if (GUILayout.Button("RESET LEVEL", style))
        {
            SceneManager.LoadScene(scene.name);
        }
    }

    public void DoMyChooseLevelWindow(int windowID3)
    {
        scene = SceneManager.GetActiveScene();

        GUILayout.Box("CURRENT LEVEL : " + scene.name, style);

        GUILayout.Box("SCORE : " + score.ToString(), style);

        GUILayout.Box("OBJECTS AVAILABLE : " + (maxObjects - usedObjects), style); 
    }

    public void DoMyWorldWindow(int windowID4) // Menu to choose level 
    {
        GUILayout.Box("CHOOSE LEVEL : ", style);

        if (GUILayout.Button("WORLD 1", style))
        {
            SceneManager.LoadScene("World 1");
        }

        if (GUILayout.Button("WORLD 2", ChangeColor(GoalScript.w1)))
        {
            if (GoalScript.w1)
            {
                SceneManager.LoadScene("World 2");
            }
        }

        if (GUILayout.Button("WORLD 3", ChangeColor(GoalScript.w2)))
        {
            if (GoalScript.w2)
            {
                SceneManager.LoadScene("World 3");
            }
        }
    }
    public void DoMyTimerWindow(int windowID5)
    {
        GUILayout.Box("BUILD TIME " + buildTimer, style);
        GUILayout.Box("PLAY TIME " + playTimer, style);
    }

    private GUIStyle ChangeColor(bool choose_color)
    {
        if(!choose_color)
        {
            return unbeatenLevelStyle;
        }

        return style;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class extaPointScript : MonoBehaviour {

    public GameObject points;
    private Scene world;
    private static bool point1 = false;
    private static bool point2 = false;
    private static bool point3 = false;
    private void Update()
    {
        world = SceneManager.GetActiveScene();
    }
    private void OnTriggerEnter()
    {
        points.SetActive(false);
        if(world.name == "World 1")
        {
            point1 = increasePoints(point1);
        }
        if (world.name == "World 2")
        {
            point2 = increasePoints(point2);
        }
        if (world.name == "World 3")
        {
            point3 = increasePoints(point3);
        }
    }

    // Awards points once per level
    //
    // @ param -  earned determines if the points have been previously awarded already
    private bool increasePoints(bool earned)
    {
        if (!earned)
        {
            GameObject.Find("GameController").GetComponent<GameControllerScript>().Score();
            GameObject.Find("GameController").GetComponent<GameControllerScript>().Score();
            earned = true;
        }
        return earned;
    }
}

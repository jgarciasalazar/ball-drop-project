﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepDistance : MonoBehaviour {

   
    public GameObject teleportExit;
    private Vector3 teleportLocation;
    private Vector3 teleportExitLocation;

    // Update is called once per frame
    private void Start()
    {
        teleportLocation = transform.position;
        teleportExitLocation = teleportExit.transform.position;
     
    }
    void Update ()
    {
        float dist = Vector3.Distance(transform.position, teleportExit.transform.position);
        if(dist > 100)
        {
            transform.position = teleportLocation;
            teleportExit.transform.position = teleportExitLocation;
        }
        
	}
}
